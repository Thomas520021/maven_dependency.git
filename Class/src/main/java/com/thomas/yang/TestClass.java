/**
 * 项目名称: maven_dependency
 * 类名称: TestClass.java
 * 包名称: com.thomas.yang
 * <p>
 * 修改履历:
 * 日期       2019-12-24
 * 修正者     Thomas
 * 主要内容   初版做成
 * <p>
 * Copyright (c) 2016-2019
 */
package com.thomas.yang;

/**
 *@Author Thomas 2019-12-24 10:07
 * The world of programs is a wonderful world
 */
public class TestClass {

    public static void main(String[] args) {

    }
}
